/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

const library = require("../../lib/content/snippets.js");
const {timeout} = require("./_utils");

// We need this stub for the injector.
window.browser = {
  runtime: {
    getURL: () => ""
  }
};

async function runSnippet(test, snippetName, ...args)
{
  let snippet = library[snippetName];

  test.ok(snippet);

  snippet(...args);

  // For snippets that run in the context of the document via a <script>
  // element (i.e. snippets that use makeInjector()), we need to wait for
  // execution to be complete.
  await timeout(100);
}

exports.testAbortOnPropertyReadSnippet = async function(test)
{
  function testProperty(property, result = true, errorName = "ReferenceError")
  {
    let path = property.split(".");

    let exceptionCaught = false;
    let value = 1;

    try
    {
      let obj = window;
      while (path.length > 1)
        obj = obj[path.shift()];
      value = obj[path.shift()];
    }
    catch (e)
    {
      test.equal(e.name, errorName);
      exceptionCaught = true;
    }

    test.equal(
      exceptionCaught,
      result,
      `The property "${property}" ${result ? "should" : "shouldn't"} trigger an exception.`
    );
    test.equal(
      value,
      result ? 1 : undefined,
      `The value for "${property}" ${result ? "shouldn't" : "should"} have been read.`
    );
  }

  window.abpTest = "fortytwo";
  await runSnippet(test, "abort-on-property-read", "abpTest");
  testProperty("abpTest");

  window.abpTest2 = {prop1: "fortytwo"};
  await runSnippet(test, "abort-on-property-read", "abpTest2.prop1");
  testProperty("abpTest2.prop1");

  // Test that we try to catch a property that doesn't exist yet.
  await runSnippet(test, "abort-on-property-read", "abpTest3.prop1");
  window.abpTest3 = {prop1: "fortytwo"};
  testProperty("abpTest3.prop1");

  // Test that other properties don't trigger.
  testProperty("abpTest3.prop2", false);

  // Test overwriting the object with another object.
  window.abpTest4 = {prop3: {}};
  await runSnippet(test, "abort-on-property-read", "abpTest4.prop3.foo");
  testProperty("abpTest4.prop3.foo");
  window.abpTest4.prop3 = {};
  testProperty("abpTest4.prop3.foo");

  // Test if we start with a non-object.
  window.abpTest5 = 42;
  await runSnippet(test, "abort-on-property-read", "abpTest5.prop4.bar");

  testProperty("abpTest5.prop4.bar", true, "TypeError");

  window.abpTest5 = {prop4: 42};
  testProperty("abpTest5.prop4.bar", false);
  window.abpTest5 = {prop4: {}};
  testProperty("abpTest5.prop4.bar");

  // Check that it works on properties that are functions.
  // https://issues.adblockplus.org/ticket/7419

  // Existing function (from the API).
  await runSnippet(test, "abort-on-property-read", "Object.keys");
  testProperty("Object.keys");

  // Function properties.
  window.abpTest6 = function() {};
  window.abpTest6.prop1 = function() {};
  await runSnippet(test, "abort-on-property-read", "abpTest6.prop1");
  testProperty("abpTest6.prop1");

  // Function properties, with sub-property set afterwards.
  window.abpTest7 = function() {};
  await runSnippet(test, "abort-on-property-read", "abpTest7.prop1");
  window.abpTest7.prop1 = function() {};
  testProperty("abpTest7.prop1");

  // Function properties, with base property as function set afterwards.
  await runSnippet(test, "abort-on-property-read", "abpTest8.prop1");
  window.abpTest8 = function() {};
  window.abpTest8.prop1 = function() {};
  testProperty("abpTest8.prop1");

  // Arrow function properties.
  window.abpTest9 = () => {};
  await runSnippet(test, "abort-on-property-read", "abpTest9");
  testProperty("abpTest9");

  // Class function properties.
  window.abpTest10 = class {};
  await runSnippet(test, "abort-on-property-read", "abpTest10");
  testProperty("abpTest10");

  // Class function properties with prototype function properties.
  window.abpTest11 = class {};
  window.abpTest11.prototype.prop1 = function() {};
  await runSnippet(test, "abort-on-property-read", "abpTest11.prototype.prop1");
  testProperty("abpTest11.prototype.prop1");

  // Class function properties with prototype function properties, with
  // prototype property set afterwards.
  window.abpTest12 = class {};
  await runSnippet(test, "abort-on-property-read", "abpTest12.prototype.prop1");
  window.abpTest12.prototype.prop1 = function() {};
  testProperty("abpTest12.prototype.prop1");

  test.done();
};
